package exercise;

class Triangle {
    // BEGIN
    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }

    public static double getSquare(int firstSide, int secondSide, int angle) {
        double angleInRadians = angle * Math.PI / 180;
        return firstSide * secondSide / 2.0 * Math.sin(angleInRadians);
    }
    // END
}
