package exercise;

class Converter {
    // BEGIN
    public static void main(String[] args) {
       System.out.println("10 Kb = " + convert(10, "b") + " b");
    }

    public static int convert(int num, String convertType) {
        int result;
        switch(convertType) {
            case "b": {
                result = num * 1024;
                break;
            }
            case "Kb": {
                result = num / 1024;
                break;
            }
            default: {
                result = 0;
                break;
            }
        }
        return result;
    }
    // END
}
