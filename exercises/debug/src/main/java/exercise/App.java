package exercise;

class App {
    // BEGIN
    /*
    Методы main и getFinalGrade для задачи из самостоятельной работы
     */
    public static void main(String[] args) {
        System.out.println(getFinalGrade(100, 12));
        System.out.println(getFinalGrade(99, 0));
        System.out.println(getFinalGrade(10, 15));
        System.out.println(getFinalGrade(85, 5));
        System.out.println(getFinalGrade(55, 3));
        System.out.println(getFinalGrade(55, 0));
    }

    public static int getFinalGrade(int exam, int project) {
        if ((exam > 90) || (project > 10)) {
            return 100;
        } else if ((exam > 75) && (project >= 5)) {
            return 90;
        } else if ((exam > 50) && (project >= 2)) {
            return 75;
        } else {
            return 0;
        }
    }

    /*
    Основная задача из ДЗ
     */
    public static String getTypeOfTriangle(int sideA, int sideB, int sideC) {
        boolean isExisting = ((sideA + sideB > sideC) && (sideB + sideC > sideA) && (sideC + sideA > sideB));

        if(isExisting) {
            boolean isVersatile = (sideA != sideB) && (sideB != sideC) && (sideA != sideC); // Разносторонний треугольник
            boolean isEquilateral = (sideA == sideB) && (sideB == sideC); // Равносторонний треугольник

            if (isVersatile) {
                return "Разносторонний";
            } else if (isEquilateral) {
                return "Равносторонний";
            } else {
                return "Равнобедренный";
            }
        } else {
            return "Треугольник не существует";
        }
    }
    // END
}
