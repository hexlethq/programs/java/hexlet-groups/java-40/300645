package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        String result = "";

        if (str.charAt(0) != ' ') { // Проверка первого символа
            result += str.charAt(0);
        }

        for (int i = 1; i < str.length(); i++) {

            if ((str.charAt(i) != ' ') && (str.charAt(i - 1) == ' ')) { // Проверка всех остальных символов кроме первого
                result += Character.toUpperCase(str.charAt(i));
            }
        }

        return result;
    }
    // END
}
